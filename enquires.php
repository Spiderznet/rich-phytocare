<?php
    /*
    THIS FILE USES PHPMAILER INSTEAD OF THE PHP MAIL() FUNCTION
    AND ALSO SMTP TO SEND THE EMAILS
    */
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require './PHPMailer/src/Exception.php';
    require './PHPMailer/src/PHPMailer.php';
    require './PHPMailer/src/SMTP.php';

    /*
    *  CONFIGURE EVERYTHING HERE
    */
    $name = $_POST["txtname"];
    $email = $_POST["txtemail"];
    $subject = $_POST["txtsubject"];
    $messages = $_POST["txtcontmsg"];

    // an email address that will be in the From field of the email.
    $fromEmail = $email;
    $fromName = $name;

    // an email address that will receive the email with the output of the form
    $sendToEmail = 'nshanmugapriya22@gmail.com';
    $sendToName = 'Rich Phytocare';

    // subject of the email
    $subject = 'New message from Enquiry Form';

    // smtp credentials and server

    $smtpHost = 'mail.richphytocare.com';
    $smtpUsername = 'enquiry@richphytocare.com';
    $smtpPassword = 'Mail@enquiry';

    // form field names and their translations.
    // array variable name => Text to appear in the email
    $fields = array('txtname' => 'Name', 'txtemail' => 'Mail', 'txtsubject' => 'Subject', 'txtcontmsg' => 'Message');

    // message that will be displayed when everything is OK :)
    $okMessage = 'Your details have been successfully submitted. Thank you, we will get back to you soon!';

    // If something goes wrong, we will display this message.
    $errorMessage = 'There was an error while submitting the form. Please try again later';


    if (count($_POST) == 0) {
        throw new \Exception('Form is empty');
    }
    
    $emailTextHtml = "<h1>You have a new message from your Enquires form</h1><hr>";
    $emailTextHtml .= "<table>";
    
    foreach ($_POST as $key => $value) {
        
        if (isset($fields[$key])) {
            $emailTextHtml .= "<tr><th>$fields[$key]</th><td>$value</td></tr>";
        }
    }
    $emailTextHtml .= "</table><hr>";

    $emailTextHtml .= "<p>Have a nice day,<br>Best,<br>Rich Phytocare</p>";
    
    $mail = new PHPMailer(true); 
    
    $mail->setFrom($fromEmail, $fromName);
    $mail->addAddress($sendToEmail, $sendToName); // you can add more addresses by simply adding another line with $mail->addAddress();
    $mail->addReplyTo($fromEmail);
    
    // $mail->isHTML(true);
    
    $mail->Subject = $subject;
    $mail->Body    = $emailTextHtml;
    $mail->msgHTML($emailTextHtml); // this will also create a plain-text version of the HTML email, very handy
    
    
    /**$mail->isSMTP();

    $mail->SMTPDebug = 0;

    $mail->Host = gethostbyname($smtpHost);
    
    $mail->Port = 587;
    
    $mail->SMTPSecure = 'tls';
    
    $mail->SMTPAuth = true;
    $mail->SMTPOptions = array (
        'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
        )
    );

    $mail->Username = $smtpUsername;
    
    //Password to use for SMTP authentication
    $mail->Password = $smtpPassword;**/
    
    if (!$mail->send()) {
        throw new \Exception('I could not send the email.' . $mail->ErrorInfo);
    }else{
        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
    
    // if requested by AJAX request return JSON response
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $encoded = json_encode($responseArray);
        
        header('Content-Type: application/json');
        
        echo $encoded;
    }

    // else just display the message
    else {
        echo $responseArray['message'];
    }
    

?>